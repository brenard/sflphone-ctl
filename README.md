# SFLphone command-line control tool

This tool permit to control your currently running SFLphone
daemon using it's DBUS API.

Feature :
- List current calls
- Make new call
- Toogle call (answer or hang up)
- Answer / Refuse / Hang up call
- Hold / Unhold call
- Transfer call
- List configured accounts

# Usage

```
Usage: sflphone-ctl [options] command [arg1] [arg2]

Control your currently running sflphone daemon via DBUS API.

Options:
  -h, --help            show this help message and exit
  -a ACCOUNT, --account=ACCOUNT
                        Account Name
  --dbus-root-name=DBUS_ROOT_NAME
                        SFLphone DBUS root API name
  -v, --verbose         Verbose mode
  -d, --debug           Debug mode

Available commands :
  - list : List current calls
  - call : Make new call
    The recipient number is specify by the second argument.
  - toogle : Answer or hang up current call 
      You could specify call ID as second argument. If no call ID is
      specify :
      - if one current call is in INCOMING state, anwser it
      - if more than one current call is in INCOMING state, fatal error
        is triggered
      - if one call is in CURRENT state, hang up it and if another call,
        the first one, is in HOLD state, unhold it.
      - if more than one call is in CURRENT state, fatal error is triggered
  - answer : Answer call
      You could specify call ID as second argument. If no call ID is
      specify, the first one call in INCOMING state is choosen.
  - refuse : Refuse call
      You could specify call ID as second argument. If no call ID is
      specify, the first one call in INCOMING state is choosen. If more
      than one call is in INCOMING state, fatal error is triggered.
  - transfer: Transfer call
      You could specify call ID as second argument. If no call ID is
      specify, the first one call in CURRENT or HOLD state is choosen.
      You must specify as second (or third) argument the recipient number.
  - hangup : Hang up call
      You could specify call ID as second argument. If no call ID is
      specify, the first one call in CURRENT or HOLD state is choosen.
      If more than one call is in CURRENT or HOLD state, fatal error is
      triggered.
  - hold : Hold call
      You could specify call ID as second argument. If no call ID is
      specify, the first one call in CURRENT state is choosen.
  - unhold : Unhold call
      You could specify call ID as second argument. If no call ID is
      specify, the first one call in HOLD state is choosen.
  - list-accounts : List configured accounts with details
```
